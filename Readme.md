# very simple random decision maker 

## more interesting parts of code : 

```
: buffer: create allot ;
variable looping
variable rndseed
20 constant strsize
decimal 100 strsize * buffer: strings
$10450405 constant generator
\  string array operators
: [$]   ( ndx buffer -- addr[i] )   swap strsize * + ; 
: [$]!  ( addr len ndx buffer -- ) [$] place ;
: [$]@ 	( ndx buffer -- addr len)  [$] count ; 
: .[$]  ( ndx buffer -- )  [$]@ type ;
```
and use random seed words : 
```
: rnd ( -- n ) rndseed @ generator um* drop 1+ dup rndseed ! ;
: random ( n -- 0..n ) rnd um* nip 1 + ;
: genrnd ( limit -- 1..limit ) begin random dup 10 <= until ;
```

## all the rest of the code is pure display & inputs

It is all based on 3 major steps 

- managing random seed

- managing inputs/output

- store & manage faked array of strings

## LICENCE

[LICENSE](./LICENSE)

## test run

```
rancois@zaphod:~/GITLAB/dev/dev_bash_decision$ gforth decision.fs                                                                                                                            
                                                                                                                                                                                              
                                                                                                                                                                                              
     Enter no more than ten (10) words of few characters maybe less than 20 each.                                                                                                             
     (others will just be ignored)                                                                                                                                                            
                                                                                                                                                                                              
                                                                                                                                                                                              
      how many words to randomize ? 3                                                                                                                                                         
                                                                                                                                                                                              
                                                                                                                                                                                              
                                                                                                                                                                                              
      word ? toto                                                                                                                                                                             
                                                                                                                                                                                              
      word ? titi                                                                                                                                                                             
                                                                                                                                                                                              
      word ? tutu                                                                                                                                                                             
                                                                                                                                                                                              
                     titi                                                                                                                                                                     
                                                                                                                                                                                              
francois@zaphod:~/GITLAB/dev/dev_bash_decision$ 
```

# Next

On future version this will be able to manage priorities on each words for better random choice.

other stuff to change _maybe one day_

- [ ] check user numbers of input

- [ ] even convert it in a loop rather than a user input

- [ ] add to user inputs _words_ a priority weight

- [X]  colorize the script interface 

# Unlicensed 

I gave that a the unlicensed status of course. For more information, please refer to [https://unlicense.org](https://unlicense.org)

