#! /usr/bin/env bash
# ____  _____ ____ ___ ____ ___ ___  _   _ 
#|  _ \| ____/ ___|_ _/ ___|_ _/ _ \| \ | |
#| | | |  _|| |    | |\___ \| | | | |  \| |
#| |_| | |__| |___ | | ___) | | |_| | |\  |
#|____/|_____\____|___|____/___\___/|_| \_|
#                                          
#    _    _     _____    _  _____ ___ ___ ____  _____ 
#   / \  | |   | ____|  / \|_   _/ _ \_ _|  _ \| ____|
#  / _ \ | |   |  _|   / _ \ | || | | | || |_) |  _|  
# / ___ \| |___| |___ / ___ \| || |_| | ||  _ <| |___ 
#/_/   \_\_____|_____/_/   \_\_| \___/___|_| \_\_____|
#                                                     
if [ $# -eq 0 ] ; then
	echo Usage : $0 NumberOfInputs
	exit 1
fi
if ! [[ $1 =~ ^-?[1-9]$ ]]; then
    echo "Error: The first argument is not a number."
    exit 1
fi
runner=$(which gforth-fast)
[ $(grep gforth-fast <<< ${runner}) ] && $runner ./decision.fs $1 || ( echo "gforth not found" && exit 1 )
exit 0
