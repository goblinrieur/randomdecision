require random.fs		\ random
DECIMAL
20 constant strsize
27 constant ESC
: buffer: create allot ;
10 strsize * buffer: strings \ might be enough for all use-cases
variable looping variable rndseed variable ilooping
\  string array operators
: [$]   ( ndx buffer -- addr[i] )  	swap strsize * + ; 
: [$]!  ( addr len ndx buffer -- ) 	[$] place ;
: [$]@ 	( ndx buffer -- addr len)  	[$] count ; 
: .[$]  ( ndx buffer -- )  			[$]@ type ;
\ usage
\ s" This is string #1"  1 strings [$]!   
\ 2 strings .[$]
: NCR { x }
	x 0 do
		cr  \ loop over Nth CR 
	loop
; \ vertical jump
: cursor \ hide or enable cursor 
	0 = if 
		.\" \e[?25l" \ hide
	else 
		.\" \e[?25h" \ display
	then 
;	\ 0 disable 1 enable cursor display
: colorize esc EMIT ." [" base @ >R 0 <# #S #> type R> base ! ." m"  ; \ ASCII TERMINAL ONLY
: input# ( -- true | false )
	0. strsize pad swap accept pad swap dup >r >number nip nip r> <> dup 0 = if 
	    nip
	then
;
: fetch-input ( - n f ) \ check if input is a number or a string
	pad strsize accept pad swap s>number? >r d>s r> 
;
: INPUT$ ( n -- addr n ) \ input string
	PAD SWAP ACCEPT PAD SWAP
;
\ here we have words for reading numbers and texts 
: wlist? ( n of s +  s  -- s ) \ get user inputs
	32 colorize 2 NCR 
	."      Enter no more than ten (10) words of few characters maybe less than 20 each." cr
	."      (others will just be ignored)" 
	2 NCR 33 colorize
	\ get number of inputs from the command line arguments
	argc @ 2 <> if 
		\ avoid missing argument with error message
		33 colorize 3 NCR ." 	Please give arg."  
		3 NCR
		1 (bye)
	else
		\ convert arg to looping variable
		1 arg s>number drop 1- looping !
		\ keep initial value for later use
		looping @ ilooping !
	then
	2 NCR \ might append behind here a check about that number & re- ask user input if it is over 10 :)
	begin
		31 colorize
		2 NCR ."       Word ? " 0 colorize strsize input$ looping @ strings [$]!	
		looping @ 1 - looping ! 
	looping @ 0 < until
;
: main
	page wlist? 2 NCR
	\ 15 spaces ilooping @ random strings .[$]  \  genrnd strings .[$]  \ really enough for a good random seed 
	15 spaces  
	utime drop seed ! 
	1000 0 do
		1000 I + random dup
		ilooping @ <= if
			1+ looping ! \ randomize - shuffle until value feet under argument max
		else 
			drop
		then
	loop	
	looping @ strings .[$]  \  displays (randomly) nth string from [$] array 
	2 NCR 
;
0 cursor main 1 cursor	\ hide cursor run main restore cursor
3 NCR 0 (bye)	\ exit properly with giving 0 exit state to system
